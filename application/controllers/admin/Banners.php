<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends CI_Controller {

	/**
	 * Index Page for this controller.
	 */

	public function __construct() {

        parent::__construct();

        $this->controller = 'admin/banners';

        $this->load->model(
            array('banners_model')
        );

        if (!$this->ion_auth->is_admin()) {
        	redirect('admin');
        }
    }

    function index(){
    	$banners = $this->banners_model->getAll();
       
        $dados = array(
            "banners" => $banners,
            "title" => 'Banners',
            "subtitle" => 'Banners cadastrados no sistema',
        );
        
        $this->load->template_admin("admin/banners/index.php", $dados);
    }

    function create(){
    	if($this->input->post()){ 

            $dados = $this->input->post();

            if($_FILES['imagem']['name'] != '' && $_FILES['imagem']['name'] != null) {
                $dados['imagem'] = $this->upload_image($_FILES['imagem']['name'], './assets/admin/images/banners/','imagem');
            }

            if($_FILES['imagem_mobile']['name'] != '' && $_FILES['imagem_mobile']['name'] != null) {
                $dados['imagem_mobile'] = $this->upload_image($_FILES['imagem_mobile']['name'], './assets/admin/images/banners/mobile', 'imagem_mobile');
            }
            
            $this->banners_model->set($dados);

            $this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <h4><i class='icon fa fa-check'></i>Cadastro feito com sucesso!</h4>
                    </div>");

            redirect($this->controller);

    	}else{
            $dados = array(
                "title" => 'Cadastrar banner',
                "subtitle" => 'Cadastrar banner no sistema',
                "action" => base_url().'admin/banners/create',
            );
            
            $this->load->template_admin("admin/banners/banner.php", $dados);
    	}
    }

    public function ativar_desativar($id = '')
    {
        if ($id != '') {

            $this->banners_model->ativar_desativar($id);

            $this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <h4><i class='icon fa fa-check'></i>Alteração feita com sucesso!</h4>
                    </div>");

            redirect($this->controller);
        }
    }

    public function update($id = '')
    {
        if ($id != '') {
               
            if ($this->input->post()) {

                $dados = $this->input->post();

                if($_FILES['imagem']['name'] != '' && $_FILES['imagem']['name'] != null) {
                    $dados['imagem'] = $this->upload_image($_FILES['imagem']['name'], './assets/admin/images/banners/', 'imagem');
                }
    
                if($_FILES['imagem_mobile']['name'] != '' && $_FILES['imagem_mobile']['name'] != null) {
                    $dados['imagem_mobile'] = $this->upload_image($_FILES['imagem_mobile']['name'], './assets/admin/images/banners/mobile', 'imagem_mobile');
                }

                $this->banners_model->update($dados, $id);

                $this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <h4><i class='icon fa fa-check'></i>Alteração feita com sucesso!</h4>
                    </div>");

                redirect($this->controller);
            }
            else{
                $banner = $this->banners_model->getById($id);

                $dados = array(
                    "action" => base_url().'admin/banners/update/'.$id,
                    "title" => "Editar banner",
                    "subtitle" => "Editar informações do banner cadastrado",
                    "banner" => $banner[0]
                );

                $this->load->template_admin("admin/banners/banner.php", $dados);
            }
        }
        else{
            redirect($this->controller);
        }
    }

    public function delete($id = '')
    {
        if ($id != '') {

            $this->banners_model->delete($id);

            $this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissible' style='margin-top: 10px; margin-bottom: 0px;'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <h4><i class='icon fa fa-check'></i>Banner excluido com sucesso!</h4>
                    </div>");

            redirect($this->controller);
        }
    }

    public function upload_image($file_name, $file_path, $file_field) {
                  
        $config['upload_path']          = $file_path;
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 1500;
        $config['max_width']            = 4000;
        $config['max_height']           = 4000;
        $config['file_name']            = $this->change_name($file_name);
    
        $path = $file_name;
        $ext = pathinfo($path, PATHINFO_EXTENSION);
    
        if ($file_name != '' && $file_name!= null) {

            $file_name = $this->change_name($file_name);
            
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload($file_field)){         
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
                die();
                redirect($this->controller);
            }
            else{
                if ($file_name != '' && $file_name != null) {
                    return $file_name;
                }
            }
        }    
    }

    public function change_name($fileName){

        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', ' ', '\'');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', '_', '-');

        $replaced = str_replace($a, $b, $fileName);
        $newName = time()."-".$replaced;

      return $newName;
    }
}