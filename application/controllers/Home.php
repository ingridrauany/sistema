<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {

        parent::__construct();

        $this->load->model(
            array('home_model', 'banners_model', 'equipe_model')
        );
    }

    function index(){

        $banners = $this->banners_model->getActives();
        $equipe = $this->equipe_model->getActives();
       
        $dados = array(
            "banners" => $banners,
            "equipe" => $equipe,
        );
        
        $this->load->template("home/index", $dados);
    }

    function sendEmail() {
        if(!$this->input->is_ajax_request()) {
            echo json_encode('false');
        } else {
            $this->form_validation->set_rules('nome', 'Nome', 'required');
            $this->form_validation->set_rules('email', 'E-mail', 'required');
            $this->form_validation->set_rules('telefone', 'Telefone', 'required');

            if ($this->form_validation->run() == FALSE) {
                echo json_encode('false');
            } else {
                $dados = $this->input->post();
                $this->home_model->set($dados);
                echo json_encode(true);
            }
        }
    }
}