<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader {
    
    public function template($nome, $dados = array()) {
        $this->view("layout/header.php");
        $this->view($nome, $dados);
        $this->view("layout/footer.php");
    }

    public function template_auth($nome, $dados = array()) {
        $this->view("auth/header.php");
        $this->view($nome, $dados);
        $this->view("auth/footer.php");
    }

    public function template_admin($nome, $dados = array()) {
        $this->view("admin/head.php");
        $this->view("admin/menu.php");
        $this->view($nome, $dados);
        $this->view("admin/footer.php");
    }
}