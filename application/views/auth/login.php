<body class="hold-transition login-page">
<div class="login-box">
  	<div class="login-logo">
    	<a href="../../index2.html"><b>Admin</b></br>SISTEMA</a>
  	</div>
	<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg">Inicie sua sessão</p>

    <form action="login" method="post">
      	<?php if(isset($message)) { ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<div id="infoMessage"><?php echo $message;?></div>
			</div>
      	<?php } ?>
      	<div class="form-group has-feedback">
        	<input type="email"  name="identity" class="form-control" placeholder="E-mail">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password" class="form-control" placeholder="Senha">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-8">
			<div class="checkbox icheck">
				<label>
				<input type="checkbox" name="remember" value="1" id="remember"> Lembrar
				</label>
			</div>
			</div>
			<!-- /.col -->
			<div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
			</div>
			<!-- /.col -->
		</div>
	</form>
	<!-- <a href="#">Esqueci minha senha</a><br> -->
	<!-- <a href="register.html" class="text-center">Cadastrar um novo usuário</a> -->

	</div>
	<!-- /.login-box-body -->
</div>