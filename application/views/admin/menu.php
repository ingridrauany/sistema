  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/admin/admin_lte/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->ion_auth->user()->row()->first_name; ?></p>
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu</li>
        <li><a href="<?php echo base_url()."admin/banners" ?>"><i class="fa fa-image"></i> <span>Banners</span></a></li>
        <li><a href="<?php echo base_url()."admin/equipe" ?>"><i class="fa fa-user"></i> <span>Equipe</span></a></li>
        <li><a href="<?php echo base_url()."admin/users" ?>"><i class="fa fa-users"></i> <span>Usuários</span></a></li>
        <li><a href="<?php echo base_url()."admin/configuracoes" ?>"><i class="fa fa-gear"></i> <span>Configurações</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->  