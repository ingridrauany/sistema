<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin/dashboard";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/configuracoes";?>">Configurações</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="box ">
            <form role="form" id="salvar" method="post" action="<?php echo $action ?>">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-building"></i> Dados da Empresa
                </h2>
				<?php if(isset($message)) { ?>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div id="infoMessage"><?php echo $message;?></div>
                </div>
				<?php } ?>
            </div>
            <div class="box-body">
                <div class="form-group col-md-4">
                    <label>Nome</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
                        <input type="text" name="nome" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->nome : '' ?>" placeholder="Nome">
                    </div>
                </div>

                <div class="form-group col-md-8">
                    <label>Descrição</label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-align-center"></i></span>
                        <input type="text" name="descricao" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->descricao : '' ?>" />
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <label>Telefone</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-phone"></i></span>
                        <input type="text" name="telefone" class="form-control tel" value="<?php echo (isset($configuracao)) ? $configuracao->telefone : '' ?>" >
                    </div>
                </div>

                <div class="form-group col-md-8">
                    <label>E-mail</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-envelope"></i></span>
                        <input type="text" name="email" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->email : '' ?>" />
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Facebook</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-facebook"></i></span>
                        <input type="text" name="facebook" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->facebook : '' ?>" >
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Instagram</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-instagram"></i></span>
                        <input type="text" name="instagram" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->instagram : '' ?>" />
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <label>Endereço</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-location-arrow"></i></span>
                        <input type="text" name="endereco" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->endereco : '' ?>" >
                    </div>
                </div>

                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> SEO
                    </h2>
                </div>

                <div class="form-group col-md-4">
                    <label>Título da página</label>
                    <input type="text" name="title" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->title : '' ?>" >
                </div>
                <div class="form-group col-md-8">
                    <label>Palavras chave da página</label>
                    <input type="text" name="keywords" class="form-control" value="<?php echo (isset($configuracao)) ? $configuracao->keywords : '' ?>" />
                </div>         
            </div>

            <div class="box-footer">
              <button type="submit" class="btn btn-primary salvar">Salvar</button>
            </div>

          </form>
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->