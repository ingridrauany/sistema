<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/users";?>">Usuários</a></li>
      </ol>
    </section>

	<section class="content">
  		<?php echo form_open("admin/users/deactivate/".$user->id);?>
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div style="background: #f39c12;">
							<?php echo $this->session->flashdata('erro');?>
						</div>
						<?php if(isset($message)) { ?>
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<div id="infoMessage"><?php echo $message;?></div>
						</div>
						<?php } ?>
						 
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group col-md-6">
										<?php echo sprintf(lang('deactivate_subheading'), $user->username);?></br>
										<div class="radio">
											<label>
												<input type="radio" name="confirm" value="yes" checked="checked" />
												Sim
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="confirm" value="no" />
												Não
											</label>
										</div>										
									</div>
								</div>
							</div>
						</div>

					<div class="box-footer">
						<?php  echo anchor('admin/users','<button type="button" class="btn btn-default">Voltar</button>'); ?>
						<button type="submit" class="btn btn-primary pull-right">Salvar</button>
					</div>
				</div>
				<?php echo form_hidden($csrf); ?>
				<?php echo form_hidden(array('id'=>$user->id)); ?>

				<!-- /.box -->
				</div>
				<!-- /.content -->
			</div>
		<?php echo form_close();?>
	</section>
</div>
<!-- /.content-wrapper -->
