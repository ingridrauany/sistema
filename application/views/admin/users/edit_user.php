<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/users";?>">Usuários</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="box">
            <?php echo form_open_multipart(uri_string());?>
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> Dados da Usuário
                </h2>
				<?php if(isset($message)) { ?>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div id="infoMessage"><?php echo $message;?></div>
                </div>
				<?php } ?>
            </div>
            <div class="box-body">
		
                <div class="form-group col-md-6">
                    <label>Nome</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
                        <input type="text" name="first_name" class="form-control" value="<?php echo $first_name['value'] ?>" placeholder="Nome">
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Sobrenome</label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
                        <input type="text" name="last_name" class="form-control" value="<?php echo $last_name['value'] ?>" />
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <label>Telefone</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-phone"></i></span>
                        <input type="text" name="phone" class="form-control tel" value="<?php echo $phone['value'] ?>" >
                    </div>
                </div>
                <div class="form-group col-md-8">
                    <label>Empresa</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-building"></i></span>
                        <input type="text" name="company" class="form-control tel" value="<?php echo $company['value'] ?>" >
                    </div>
                </div>

                  <div class="form-group col-md-12">
                        <label>Grupos: </label>
                        <div class="input-group">
                              <?php foreach ($groups as $group):?>
                                    <span>
                                    <?php
                                          $gID=$group['id'];
                                          $checked = null;
                                          $item = null;
                                          foreach($currentGroups as $grp) {
                                                if ($gID == $grp->id) {
                                                      $checked= ' checked="checked"';
                                                break;
                                                }
                                          }
                                    ?>
                                    <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                                    <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></br>
                                    </span>
                              <?php endforeach?>
                        </div>
                  </div>

                <div class="form-group col-md-6">
                    <label>Senha</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
                        <input type="password" name="password" class="form-control" value="" >
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Confirmação de senha</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
                        <input type="password" name="password_confirm" class="form-control" value="" />
                    </div>
                </div>

                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Imagem do perfil</label>
                  <input name="image" type="file" id="exampleInputFile">
                </div>

                <div class="form-group col-md-12">
                    <?php echo (isset($image) && ($image['value'] != '' && $image['value'] != null)) ? "<img style='max-width: 200px;' src='".base_url()."assets/admin/images/users/".$image['value']."'>" : '' ?>
                </div>
            </div>

            <?php echo form_hidden('id', $user->id);?>
            <?php echo form_hidden($csrf); ?>

            <div class="box-footer">
                  <?php  echo anchor('admin/users','<button type="button" class="btn btn-default">Voltar</button>'); ?>
                  <button type="submit" class="btn btn-primary pull-right">Salvar</button>
            </div>
          
            <?php echo form_close();?>
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
