<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/users";?>">Usuários</a></li>
        <li><a href="<?php echo base_url()."admin/users/groups";?>">Grupos de Usuários</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="box">
        <?php echo form_open(current_url());?>
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-users"></i> Dados da Grupo
                </h2>
				<?php if(isset($message)) { ?>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div id="infoMessage"><?php echo $message;?></div>
                </div>
				<?php } ?>
            </div>
            <div class="box-body">
		
                <div class="form-group col-md-4">
                    <label>Nome</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
                        <input type="text" name="group_name" class="form-control" value="<?php echo $group_name['value'] ?>" placeholder="Nome">
                    </div>
                </div>

                <div class="form-group col-md-8">
                    <label>Descrição</label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-align-center"></i></span>
                        <input type="text" name="group_description" class="form-control" value="<?php echo $group_description['value'] ?>" />
                    </div>
                </div>
            </div>

            <div class="box-footer">
                  <?php  echo anchor('admin/users/groups','<button type="button" class="btn btn-default">Voltar</button>'); ?>
                  <button type="submit" class="btn btn-primary pull-right">Salvar</button>
            </div>
          
            <?php echo form_close();?>
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
