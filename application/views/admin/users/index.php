<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/users";?>">Usuários</a></li>
      </ol>
    </section>
    <!-- /.content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<?php  echo anchor('admin/users/groups','<button type="button" class="btn btn-default"><i class="fa fa-users"></i> Grupo de usuários</button>'); ?>
						<h3 class="box-title pull-right"><?php echo anchor('admin/users/create_user', '<i class="fa fa-plus"></i> Cadastrar usuário'. lang('users_create_user'), array('class' => 'btn btn-block btn-success')); ?></h3>
					</div>
					
					<div class="box-body">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Nome</th>
									<th>E-mail</th>
									<th>Grupos</th>
									<th>Ações</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($users as $user):?>
								<tr>
									<td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
									<td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
									<td>
									<?php
									foreach ($user->groups as $group)
									{
										echo anchor('admin/users/edit_group/'.$group->id, '<span class="label label-warning">'.htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8').'</span> ');
									}
									?>
									</td>
									<td>
										<?php echo anchor('admin/users/edit_user/'.$user->id.'','<button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></button>');?>
									</td>
									<td>
										<?php 
											echo ($user->active) ? anchor('admin/users/deactivate/'.$user->id.'','<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>') : anchor('admin/users/activate/'.$user->id.'','<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i></button>');
										?>
									</td>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>