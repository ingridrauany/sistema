<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/users";?>">Usuários</a></li>
        <li><a href="<?php echo base_url()."admin/users/groups";?>">Grupos</a></li>
      </ol>
    </section>
    <!-- /.content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<?php  echo anchor('admin/users','<button type="button" class="btn btn-default"><i class="fa fa-users"></i> Lista de usuários</button>'); ?>
						<h3 class="box-title pull-right"><?php echo anchor('admin/users/create_group', '<i class="fa fa-plus"></i> Cadastrar grupo'. lang('users_create_group'), array('class' => 'btn btn-block btn-success')); ?></h3>
					</div>
					
					<div class="box-body">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Descrição</th>
                                    <th>Ações</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($groups as $group):?>
								<tr>
									<td><?php echo htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8'); ?></td>
									<td><?php echo htmlspecialchars($group->description, ENT_QUOTES, 'UTF-8'); ?></td>
									<td>
										<?php echo anchor('admin/users/edit_group/'.$group->id.'','<button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></button>');?>
									</td>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>