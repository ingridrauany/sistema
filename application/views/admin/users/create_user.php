<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/users";?>">Usuários</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="box">
            <?php echo form_open_multipart("admin/users/create_user");?>
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> Dados da Usuário
                </h2>
				<?php if(isset($message)) { ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<div id="infoMessage"><?php echo $message;?></div>
					</div>
				<?php } ?>
            </div>
            <div class="box-body">
		
                <div class="form-group col-md-6">
                    <label>Nome</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
                        <input type="text" name="first_name" class="form-control" value="" placeholder="Nome">
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Sobrenome</label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
                        <input type="text" name="last_name" class="form-control" value="" />
                    </div>
                </div>

                <?php
					if($identity_column!=='email') {
						echo '<p>';
						echo lang('create_user_identity_label', 'identity');
						echo '<br />';
						echo form_error('identity');
						echo form_input($identity);
						echo '</p>';
					}
                ?>

                <div class="form-group col-md-6">
                    <label>E-mail</label>
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="email" name="email" class="form-control tel" value="" >
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Empresa</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-building"></i></span>
                        <input type="text" name="company" class="form-control tel" value="" >
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <label>Telefone</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-phone"></i></span>
                        <input type="text" name="phone" class="form-control tel" value="" >
                    </div>
                </div>

				<div class="form-group col-md-12"></div>


                <div class="form-group col-md-6">
                    <label>Senha</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
                        <input type="password" name="password" class="form-control" value="" >
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Confirmação de senha</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
                        <input type="password" name="password_confirm" class="form-control" value="" />
                    </div>
                </div>

                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Imagem do perfil</label>
                  <input name="image" type="file" id="exampleInputFile">
                </div>
            </div>

            <div class="box-footer">
                  <?php  echo anchor('admin/users','<button type="button" class="btn btn-default">Voltar</button>'); ?>
                  <button type="submit" class="btn btn-primary pull-right">Salvar</button>
            </div>
          
            <?php echo form_close();?>
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

