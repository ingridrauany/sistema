<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/banners";?>">Banners</a></li>
      </ol>
    </section>
    <!-- /.content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title pull-right"><?php echo anchor('admin/banners/create', '<i class="fa fa-plus"></i> Cadastrar banner'. lang('users_create_user'), array('class' => 'btn btn-block btn-success')); ?></h3>
					</div>
					
					<div class="box-body">
					<?php if(!empty($banners)) { ?>
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Título</th>
									<th>Imagem</th>
									<th>Imagem mobile</th>
									<th>Ações</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($banners as $banner):?>
								<tr>
									<td><?php echo $banner->nome; ?></td>
									<td>
									<?php 
										if ($banner->imagem != '' && $banner->imagem != null) {
											echo '<img style="height:40px;" src="'.base_url().'assets/admin/images/banners/'.$banner->imagem.'">';
										} else {
											echo 'Nenhuma imagem cadastrada.';
										}
									?>
									</td>
                  					<td>
									<?php 
										if ($banner->imagem_mobile != '' && $banner->imagem_mobile != null) {
											echo '<img style="height:40px;" src="'.base_url().'assets/admin/images/banners/mobile/'.$banner->imagem_mobile.'">';
										} else {
											echo 'Nenhuma imagem cadastrada.';
										}
									?>
									</td>
									<td>
										<?php echo anchor('admin/banners/update/'.$banner->id.'','<button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></button> ');?>
                    					<?php echo anchor('admin/banners/delete/'.$banner->id.'','<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>');?>
									</td>
									<td>
										<?php 
											echo ($banner->ativo == 1) ? anchor('admin/banners/ativar_desativar/'.$banner->id.'','<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>') : anchor('admin/banners/ativar_desativar/'.$banner->id.'','<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i></button>');
										?>
									</td>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
						<?php } else { ?>
						<p>Nenhum banner cadastrado.</p>
						<?php } ?>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>