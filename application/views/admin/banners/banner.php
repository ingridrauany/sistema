<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/banners";?>">Banners</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="box">
            <form method="post" action="<?php echo $action ?>" name="create_user" enctype="multipart/form-data"/>
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-image"></i> Dados da Banner
                </h2>
				<?php if(isset($message)) { ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<div id="infoMessage"><?php echo $message;?></div>
					</div>
				<?php } ?>
            </div>
            <div class="box-body">
		
                <div class="form-group col-md-4">
                    <label>Título</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-align-center"></i></span>
                        <input type="text" name="nome" class="form-control" value="<?php echo (isset($banner)) ? $banner->nome : '' ?>" placeholder="Título do Banner" required>
                    </div>
                </div>

                <div class="form-group col-md-8">
                    <label>Descrição</label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-align-center"></i></span>
                        <input type="text" name="descricao" class="form-control" value="<?php echo (isset($banner)) ? $banner->descricao : '' ?>" placeholder="Descrição do banner" />
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Link</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-link"></i></span>
                        <input type="text" name="link" class="form-control tel" value="<?php echo (isset($banner)) ? $banner->link : '' ?>" >
                    </div>
                </div>

                <div class="form-group col-md-12">
                  <label for="exampleInputFile">Imagem desktop</label>
                  <input name="imagem" value="" type="file">
                </div>

                <div class="form-group col-md-12">
                    <?php echo (isset($banner) && ($banner->imagem != '' && $banner->imagem != null)) ? "<img style='max-width: 200px;' src='".base_url()."assets/admin/images/banners/".$banner->imagem."'>" : '' ?>
                </div>

                <div class="form-group col-md-12">
                  <label for="exampleInputFile">Imagem mobile</label>
                  <input name="imagem_mobile" type="file">
                </div>

                <div class="form-group col-md-12">
                    <?php echo (isset($banner) && ($banner->imagem_mobile != '' && $banner->imagem_mobile != null)) ? "<img style='max-width: 200px;' src='".base_url()."assets/admin/images/banners/mobile/".$banner->imagem_mobile."'>" : '' ?>
                </div>
            </div>

            <div class="box-footer">
                  <?php  echo anchor('admin/banners','<button type="button" class="btn btn-default">Voltar</button>'); ?>
                  <button type="submit" class="btn btn-primary pull-right">Salvar</button>
            </div>
          
            <?php echo form_close();?>
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

