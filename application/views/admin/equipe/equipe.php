<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/equipe";?>">Equipe</a></li>
      </ol>
    </section>

    <section class="content">
        <div class="box">
            <form method="post" action="<?php echo $action ?>" name="create_user" enctype="multipart/form-data"/>
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-image"></i> Dados da Funcionário
                </h2>
				<?php if(isset($message)) { ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<div id="infoMessage"><?php echo $message;?></div>
					</div>
				<?php } ?>
            </div>
            <div class="box-body">
		
                <div class="form-group col-md-6">
                    <label>Nome</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-align-center"></i></span>
                        <input type="text" name="nome" class="form-control" value="<?php echo (isset($equipe)) ? $equipe->nome : '' ?>" placeholder="Nome do funcionário" required>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Título</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-align-center"></i></span>
                        <input type="text" name="titulo" class="form-control" value="<?php echo (isset($equipe)) ? $equipe->titulo : '' ?>" placeholder="Título do Funcionário" required>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Descrição</label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-align-center"></i></span>
                        <input type="text" name="descricao" class="form-control" value="<?php echo (isset($equipe)) ? $equipe->descricao : '' ?>" placeholder="Descrição do funcionário" />
                    </div>
                </div>

                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Imagem</label>
                  <input name="imagem" value="" type="file">
                </div>

                <div class="form-group col-md-12">
                    <?php echo (isset($equipe) && ($equipe->imagem != '' && $equipe->imagem != null)) ? "<img style='max-width: 200px;' src='".base_url()."assets/admin/images/equipe/".$equipe->imagem."'>" : '' ?>
                </div>
            </div>

            <div class="box-footer">
                  <?php  echo anchor('admin/equipe','<button type="button" class="btn btn-default">Voltar</button>'); ?>
                  <button type="submit" class="btn btn-primary pull-right">Salvar</button>
            </div>
          
            <?php echo form_close();?>
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

