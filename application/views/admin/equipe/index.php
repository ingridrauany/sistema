<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small><?php echo $subtitle; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin";?>"><i class="fa fa-dashboard"></i> Início</a></li>
        <li><a href="<?php echo base_url()."admin/equipe";?>">Equipe</a></li>
      </ol>
    </section>
    <!-- /.content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title pull-right"><?php echo anchor('admin/equipe/create', '<i class="fa fa-plus"></i> Cadastrar funcionário'. lang('users_create_user'), array('class' => 'btn btn-block btn-success')); ?></h3>
					</div>
					
					<div class="box-body">
					<?php if(!empty($equipe)) { ?>
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Título</th>
									<th>Imagem</th>
									<th>Ações</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($equipe as $e):?>
								<tr>
									<td><?php echo $e->nome; ?></td>
									<td><?php echo $e->titulo; ?></td>
									<td>
									<?php 
										if ($e->imagem != '' && $e->imagem != null) {
											echo '<img style="height:40px;" src="'.base_url().'assets/admin/images/equipe/'.$e->imagem.'">';
										} else {
											echo 'Nenhuma imagem cadastrada.';
										}
									?>
									</td>
									<td>
										<?php echo anchor('admin/equipe/update/'.$e->id.'','<button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></button> ');?>
                    					<?php echo anchor('admin/equipe/delete/'.$e->id.'','<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>');?>
									</td>
									<td>
										<?php 
											echo ($e->ativo == 1) ? anchor('admin/equipe/ativar_desativar/'.$e->id.'','<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>') : anchor('admin/equipe/ativar_desativar/'.$e->id.'','<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i></button>');
										?>
									</td>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
						<?php } else { ?>
						<p>Nenhum funcionário cadastrado.</p>
						<?php } ?>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>