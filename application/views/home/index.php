<div class="container">
    <div class="banner banner_home">
        <?php foreach($banners as $banner) { ?>
            <div class="banner__item">
                <img src="<?php echo base_url() .'assets/admin/images/banners/'.$banner->imagem ?>" alt="">
                <div class="banner__caption">
                    <div class="caption__text">
                        <?php echo $banner->descricao; ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="content content__quemsomos" id="quem_somos">
        <div class="content__box">
            <h2>Quem Somos</h2>

            <div class="content__text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sit amet turpis ornare lacus consectetur viverra. Morbi eleifend, elit vitae rhoncus ullamcorper, urna sapien blandit lectus, ac maximus nisl risus eu tortor. Pellentesque ante diam, tincidunt vel magna a, rhoncus volutpat mauris. Aenean ut risus maximus, scelerisque nulla in, laoreet neque. Nullam suscipit nulla elit, eget dapibus massa viverra id. Sed scelerisque vel eros sed convallis. Aliquam risus augue, aliquet nec fringilla eget, aliquet eget lacus. Aliquam in tempus sem, eu rhoncus tortor. Vestibulum eu nulla a quam molestie interdum eu nec mi. Donec vehicula magna eget suscipit interdum. Etiam tincidunt ac velit et tempus. Integer ut dui quam. Sed id risus vitae ipsum tincidunt auctor sit amet eget ligula.
            </div>

            <a href="#" class="quemsomos__btn">Saiba Mais</a>
        </div>
    </div> 

    <div class="content content__equipe" id="equipe">
        <div class="content__box">
            <h2>Equipe</h2>
            <div class="equipe__box">
                <?php foreach($equipe as $e) { ?>
                <div class="equipe__item">
                    <i class="far fa-user-circle fa-5x"></i>
                    <div class="equipe__name"><?php echo $e->nome;?></div>
                    <div class="equipe__cargo"><?php echo $e->titulo;?></div>
                </div>
                <? } ?>
            </div>
        </div>
    </div>

    <div class="content content__faleconosco" id="fale_conosco">
        <div class="validation_msg"></div>
        <div class="content__box">
            <h2>Fale Conosco</h2>
            <form method="" action="<?php echo base_url() ?>sendEmail" class="contato__box jsform">
                <input type="text" name="nome" placeholder="Seu nome" required>
                <input type="text" name="email" placeholder="Seu e-mail" required>
                <input type="text" name="telefone" placeholder="Seu Telefone" required>
                <input type="text" name="assunto" placeholder="Assunto do e-mail" required>
                <textarea name="mensagem" id="" cols="30" rows="8" placeholder="Digite aqui sua mensagem..."></textarea>
                <button>Enviar</button>
            </form>
        </div>
    </div>

    <!-- <div class="content contet__newsletter">
        <div class="content__box">

        </div>
    </div> -->
</div>