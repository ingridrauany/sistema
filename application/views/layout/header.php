<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sistema - Front-End</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
</head>
<body>
    <header>
		<div class="topo">
			<h1 class="topo__logo">
				<a href="/sistema" class="logo_img">
					Sistema
				</a>
			</h1>

			<div class="topo__menu">
				<input type="checkbox" id="control-nav" />
				<label for="control-nav" class="control-nav"></label>
				<label for="control-nav" class="control-nav-close"></label>

				<nav class="nav">
					<ul class="nav__menu">
						<li class="menu__item">
							<a href="#quem_somos" title="Quem Somos">Quem Somos</a>
						</li>
						<li class="menu__item">
							<a href="#equipe" title="Equipe">Equipe</a>
						</li>
						<li class="menu__item">
							<a href="#fale_conosco" title="Fale Conosco">Fale Conosco</a>
						</li>
					</ul>
				</nav>
			</div>

			<div class="topo__login">
				<a href="<?php echo base_url()?>admin" target="_blank">Área Adminsitrativa</a>
			</div>
		</div>
		
	</header>