<footer>
    <div class="footer__content">
        Copyright © Ingrid Rauany 2018 - Direitos reservados
    </div>
</footer>
</body>
<!-- load JQUERY -->
<script src="<?php echo base_url()?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $(".validation_msg").click(function(){
            $(this).fadeOut();
        });

        //call function for insert data
        $('.jsform').on('submit', function(form) {
            form.preventDefault();
            $.post('<?php echo base_url() ?>home/sendEmail', $('.jsform').serialize(), function(data){
                if(data == "true") {
                    $(".validation_msg").append("Mensagem enviada com sucesso!</br>Obrigada por entrar em contato.</br>Retornaremos sua mensagem em breve.");
                    $(".validation_msg").css({opacity: 0, display: 'flex'}).animate({opacity: 1}, 800);
                } else {
                    $(".validation_msg").append("Erro ao enviar a mensagem :(</br>Tente novamente!");
                    $(".validation_msg").css({opacity: 0, display: 'flex'}).animate({opacity: 1}, 800);
                }
            });
        });
    });
</script>

</html>