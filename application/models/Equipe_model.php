<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Equipe_model extends CI_Model {

	public function getAll() {
        $query = $this->db->select('*')
            ->where('excluido', 0)
            ->get('equipe');

        return  $query->result();
	}

	public function getActives() {
        $query = $this->db->select('*')
            ->where('ativo', 1)
			->where('excluido', 0)
            ->get('equipe');

        return  $query->result();
	}

	public function set($dados) {
		$this->db->insert('equipe', $dados);
	}

	public function ativar_desativar($id = '') {
		if ($id != '') {
			$query = $this->db->select('ativo')
			->where('id', $id)
			->get('equipe');

			$result = $query->result();

			if (isset($result[0])) {
				if ($result[0]->ativo == 1) {
					$this->db->where('id', $id)
					->update('equipe', array('ativo' => 0));
				}
				elseif ($result[0]->ativo == 0) {
					$this->db->where('id', $id)
					->update('equipe', array('ativo' => 1));
				}
			}
		}
	}

	public function getById($id = '') {
		if ($id != '') {
			$query = $this->db->select('*')
			    ->where('id', $id)
			    ->get('equipe');

			return $query->result();
		}
	}

	public function update($dados = '', $id = '') {
		if ($dados != '' && $id != '') {
			$this->db->where('id', $id)
			    ->update('equipe', $dados);
		}
	}

	public function delete($id = '') {
		if ($id != '') {
			$this->db->where('id', $id)
			    ->update('equipe', array('excluido' => 1));
		}
	}
}