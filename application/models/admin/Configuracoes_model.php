<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Configuracoes_model extends CI_Model {

	public function set($dados = '')
	{
		if ($dados != '') {
			$this->db->where('id', 1)
			->update('configuracoes', $dados);
		}
	}

	public function get()
	{
		$query = $this->db->select('*')
		->where('id', 1)
		->get('configuracoes');

		return $query->result();
	}
}