<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends CI_Model {

	public function getAll() {
        $query = $this->db->select('*')
            ->where('excluido', 0)
            ->get('banner');

        return  $query->result();
    }
    
	public function set($dados) {
		return ($this->db->insert('contato', $dados));
	}
}